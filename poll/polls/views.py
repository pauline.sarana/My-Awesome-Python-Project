from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from .models import Choice, Question

# Create your views here.

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:10]

class DetailView(generic.DetailView):
    # for the viewing of choices and results
    model = Question
    template_name = 'polls/detail.html'

class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'

class AddQuestion(generic.View):

    def post(self, request, *args, **kwargs):
        # function used for the addition of question
        question_text = request.POST.get('question_text')
        question = Question.objects.create(question_text = question_text, pub_date = timezone.now())
        i = 1;
        choicelist = []
        while(request.POST.get('c'+str(i))):
            choicelist.append(request.POST.get('c'+str(i)))
            i += 1

        for choice in choicelist:
            question.choice_set.create(choice_text=choice, votes=0)

        return HttpResponseRedirect(reverse('polls:index', args=()))

class Vote(generic.View):

    def post(self, request, question_id):
        # for the update of results after voting
        question = get_object_or_404(Question, pk=question_id)
        try:            
            selected_choice = question.choice_set.get(pk=request.POST['choice'])
        except (KeyError, Choice.DoesNotExist):
            return render(request, 'polls/detail.html', {
                'question' : question,
                'error_message' : "You didn't select a choice",
            })
        else:
            selected_choice.votes += 1
            selected_choice.save()
            choice_list = []
            choices = Choice.objects.filter(question=question).values('votes','choice_text');
            for choice in choices:
                choice_list.append(choice)
            choice_set = choice_list
            return JsonResponse({'status': 'success',
                                'choice_set': choice_set})