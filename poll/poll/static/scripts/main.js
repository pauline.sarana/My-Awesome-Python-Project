//requirejs inclusion of needed js files
require([
    'jquery',
    'mustache',
    'choices'
], function($,Mustache) {
    $( "#practice" ).click(function() {
        alert( "Handler for .click() called." );
    });

    $('#vote').on('submit',function(e){
        e.preventDefault();
        var comment_template =  "{{#choice_set}}"
                                +"<li>{{ choice_text }} -- {{ votes }} vote{{ votes|pluralize }}</li>"
                                +"{{/choice_set}}"

         // clear the existing list
        $('#results li').remove();
        // populate list with updated one
        $.ajax({
            type: 'POST',
            url: $('#vote').attr('action'),
            data:{
                choice :  $("input[name='choice']:checked").val(),
                csrfmiddlewaretoken:$('input[name=csrfmiddlewaretoken]').val()
            },
            success:function(data){
                if(data.status=='failed'){
                    alert('Failed')
                }
                else{
                    $('#results').append(Mustache.render(comment_template,data));
                }
            }
        });
    });


});

