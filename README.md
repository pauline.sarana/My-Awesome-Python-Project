This project is a compliance to the preliminary requirement for the part-time job in ChannelFix.

This project has an objective to create a simple yet creative poll system using Python and Django. With further integrations of RequireJS, MustacheJS, and Less.

Room for Improvements
    -appearance
    -more features for the site to be understood better
    
Notes
    -on sign-up, password should be atleast 8 characters, and must have atleast a number